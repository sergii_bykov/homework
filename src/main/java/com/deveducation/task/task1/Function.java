package com.deveducation.task.task1;

import java.util.Arrays;

public class Function {
    //  Получить строковое название дня недели по номеру дня.
    public static String getDayOfWeek(int a) {
        String review;
        switch (a) {
            case 1:
                review = "Понедельник";
                break;
            case 2:
                review = "Вторник";
                break;
            case 3:
                review = "Среда";
                break;
            case 4:
                review = "Четверг";
                break;
            case 5:
                review = "Пятница";
                break;
            case 6:
                review = "Суббота";
                break;
            case 7:

                review = "Воскресенье";
                break;
            default:
                throw new IndexOutOfBoundsException();
        }
        return review;
    }

    //  Найти расстояние между двумя точками в двумерном декартовом пространстве.
    public static double calculateDistance(int x1, int y1, int x2, int y2) {
        return Math.sqrt((Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2)));
    }

    //  Вводим число (0-999), получаем строку с прописью числа.

    public static String NumberWriter(int number) {
        final String[] BELOW_TWENTY = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        final String[] TENS = {"сто", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        final String[] THOUSANDS = {"тысяча", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        while (true) {
            if (number < 0 || number > 1000) {
                System.out.println("Попробуй ещё раз...");
                continue;
            }

            if (number < 20)
                return (BELOW_TWENTY[number]);
            else if (number > 10 && number < 100) {
                int high = number / 10;
                int low = number % 10;
                String text = TENS[high];
                if (low != 0)
                    text = text + " " + BELOW_TWENTY[low];
                return (text);

            } else if (number > 100 && number < 1000) {
                int high = number / 100;
                int highed = (number - 100 * high) / 10;
                int low = number % 10;
                String text = THOUSANDS[high];
                if (low != 0)
                    text = text + " " + TENS[highed] + " " + BELOW_TWENTY[low];
                return (text);
            } else if (number == 100) {
                return (TENS[0]);
            }
        }
    }

    //  Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число

    public static int WriteNumber(String write) {

        final String[] BELOW_TWENTY = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        final String[] TENS = {"сто", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        final String[] THOUSANDS = {"тысяча", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        String[] subStr;
        String delimeter = " "; // Разделитель
        subStr = write.split(delimeter);
        write = write.replaceAll("( )+", " "); // убираем лишние пробеллы
        String s = write.trim();
        int result = 0;
        //Начальное количество слов равно 0
        int count = 0;

        //Если ввели хотя бы одно слово, тогда считать, иначе конец программы
        if (s.length() != 0) {
            count++;
            //Проверяем каждый символ, не пробел ли это
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == ' ') {
                    //Если пробел - увеличиваем количество слов на 1
                    count++;
                }
                if (count == 3) {
                    String s1 = s.split(" ")[2];
                    String s2 = s.split(" ")[1];
                    String s3 = s.split(" ")[0];


                    result = (Arrays.asList(THOUSANDS).indexOf(s3) * 100 + (Arrays.asList(TENS).indexOf(s2)) * 10 + (Arrays.asList(BELOW_TWENTY).indexOf(s1)));
                } else if (count == 2) {
                    String s1 = s.split(" ")[1];
                    String s2 = s.split(" ")[0];
                    String s3 = s.split(" ")[0];
                    int s4 = (Arrays.asList(TENS).indexOf(s2) * 10 + (Arrays.asList(BELOW_TWENTY).indexOf(s1)));
                    int s5 = (Arrays.asList(THOUSANDS).indexOf(s3) * 100 + (Arrays.asList(BELOW_TWENTY).indexOf(s1)));
                    int s6 = (Arrays.asList(THOUSANDS).indexOf(s3) * 100 + (Arrays.asList(TENS).indexOf(s1)) * 10);

                    if (s4 > s5) {
                        result = s4;

                    } else if (s6 < s5) {
                        result = s5;
                    } else
                        result = s6;

                } else if (count == 1) {
                    String s1 = s.split(" ")[0];
                    String s2 = s.split(" ")[0];
                    String s3 = s.split(" ")[0];

                    int s4 = (Arrays.asList(TENS).indexOf(s2));
                    int s5 = (Arrays.asList(THOUSANDS).indexOf(s3));
                    int s6 = (Arrays.asList(BELOW_TWENTY).indexOf(s1));
                    if (s4 > s5) {
                        result = s4 * 10;
                    } else if (s5 > s4) {
                        result = s5 * 100;
                    } else
                        result = s6;
                }


            }
        }
        return result;

    }

}


