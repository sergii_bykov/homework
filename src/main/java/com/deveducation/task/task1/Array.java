package com.deveducation.task.task1;

public class Array {
    //  Найти минимальный элемент массива
    public static int minElement (int[] arr)
    {
        int
                review = arr[0];
        for (int i = 1; i < arr.length; i++)
        {
            if (arr[i] < review)
            {
                review = arr[i];
            }
        }
        return review;
    }

    //  Найти максимальный элемент массива
    public static int maxElement (int[] arr)
    {
        int review = arr[0];
        for (int i = 1; i < arr.length; i++)
        {
            if (arr[i] > review)
            {
                review = arr[i];
            }
        }
        return review;
    }

    //  Найти индекс минимального элемента массива
    public static int indexMinElement(int[] arr)
    {
        int review = 0, minValue = arr[0];
        for (int i = 1; i < arr.length; i++)
        {
            if (arr[i] < minValue)
            {
                review = i;
                minValue = arr[i];
            }
        }
        return review;
    }

    //  Найти индекс максимального элемента массива
    public static int indexMaxElement(int[] arr)
    {
        int review = 0, maxValue = arr[0];
        for (int i = 1; i < arr.length; i++)
        {
            if (arr[i] > maxValue)
            {
                review = i;
                maxValue = arr[i];
            }
        }
        return review;
    }
    //  Посчитать сумму элементов массива с нечетными индексами
    public static int sumElementsOddIndex(int[] arr)
    {
        int review = 0;
        for (int i = 1; i < arr.length; i++)
        {
            if (i % 2 == 0);

            else
                review += arr[+i];

        }
        return review;
    }
    //  Сделать реверс массива (массив в обратном направлении)
    public static int[] reversArray(int[] arr)
    {
        int review[] = new int[arr.length];
        int currentIndex = arr.length - 1;
        for (int i = 0; i <= (arr.length - 1); currentIndex--, i++)
        {
            review[currentIndex] = arr[i];
        }
        return review;
    }

    //  Посчитать количество нечетных элементов массива
    public static int countOddElements(int[] arr)
    {
        int review = 0;
        for (int currentElement : arr)
        {
            if (currentElement % 2 != 0)

                review++;

        }
        return review;
    }
    //  Поменять местами первую и вторую половину массива, например, для массива 1 2 3 4, результат 3 4 1 2
    public static int[] replaceHalfArray(int[] arr)
    {
        final int len = arr.length / 2;
        final int offset = arr.length - len;
        for (int i = 0; i < len; i++) {
            int temp = arr[i];
            arr[i] = arr[offset + i];
            arr[offset + i] = temp;
        }
        return arr;
    }


    //  Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))
    public static int[] bubbleSort(int[] arr)
    {
        for (int lengthIn = arr.length; lengthIn > 0; lengthIn--)
        {
            for (int swap = 0; swap < lengthIn-1; swap++)
            {
                if (arr[swap] > arr[swap + 1])
                {
                    int element  = arr[swap];
                    arr[swap] = arr[swap + 1];
                    arr[swap + 1] = element;
                }
            }
        }
        return arr;
    }

    public static int[] selectionSort(int[] arr)
    {
        int min;

        for(int OutIterations = 0; OutIterations < arr.length - 1; OutIterations++)
        {
            min = OutIterations;
            for(int CountInIterations = OutIterations; CountInIterations < arr.length; CountInIterations++)
            {
                if(arr[CountInIterations] < arr[min] )
                {
                    min = CountInIterations;
                }
            }
            int temp  = arr[OutIterations];
            arr[OutIterations] = arr[min];
            arr[min] = temp;
        }
        return arr;
    }

    public static int[] insertionSort(int[] arr)
    {
        {
            int j, value;

            for(int i = 1; i < arr.length; i++)
            {
                value = arr[i];
                for (j = i - 1; j >= 0 && arr[j] > value; j--)
                {
                    arr[j + 1] = arr[j];
                }
                arr[j + 1] = value;
            }
        }
        return arr;
    }
}
