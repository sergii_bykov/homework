package com.deveducation.task.task1;

public class ConditionalStatements {


    //Если а – четное посчитать а*б, иначе а+б
    public static int evenOdd1(int a, int b) {


        // Если число делится на 2, то это четное число, иначе - нечетное
        // если остаток от деления равен нулю, то четное
        if (a % 2 == 0)
            //число четное
            return (a * b);
        else
            //число нечетное
            return (a + b);

    }

    //Определить какой четверти принадлежит точка с координатами (х,у)
    public static int quarter2(int x, int y) {
        int a = 1;
        int b = 2;
        int c = 3;
        int d = 4;
        int e = 0;

        if (x > 0 && y > 0) {
            return a;
        } else if (x < 0 && y > 0) {
            return b;
        } else if (x < 0 && y < 0) {
            return c;
        } else if (x > 0 && y < 0) {
            return d;
        } else
            return e;
    }

    //  Найти суммы только положительных из трех чисел
    public static int sumOfPositiveElements3(int a, int b, int c) {


        int rewiew = 0;
        if (a > 0) {
            rewiew += a;
        }
        if (b > 0) {
            rewiew += b;
        }
        if (c > 0) {
            rewiew += c;
        }
        return rewiew;
    }

    //Посчитать выражение (макс(а*б*с, а+б+с))+3
    public static int expression4(int a, int b, int c) {

        int g = a * b * c;
        int h = a + b + c;
        int m = Math.max(g, h);

        return (m + 3);
    }

    //  Написать программу определения оценки студента по его рейтингу, на основе следующих правил
    public static char findStudentGrade5(int a) {
        char review = 0;
        if (a >= 0 && a <= 19) {
            review = 'F';
        } else if (a >= 20 && a <= 39) {
            review = 'E';
        } else if (a >= 40 && a <= 59) {

            review = 'D';
        } else if (a >= 60 && a <= 74) {
            review = 'C';
        } else if (a >= 75 && a <= 89) {
            review = 'B';
        } else if (a >= 90 && a <= 100) {
            review = 'A';
        }
        return review;
    }
}




