package com.deveducation.task.task1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionTest {


    @Test
    public void minElement() {
        String result = Function.getDayOfWeek(2);
        assertEquals(result, "Вторник");

    }

    @Test
    public void calculateDistance() {
        double result = Function.calculateDistance(-1, 1, 2, 5);
        assertEquals(result, 5, 0);

    }

    @Test
    public void NumberWriter() {
        String actual = "сто" + "двадцать" + "пять";
        String result = Function.NumberWriter(125);
        assertEquals(result, actual);

    }

    @Test
    public void WriteNumber() {
        int actual = 125;
        int result = Function.WriteNumber("сто двадцать пять");
        assertEquals(result, actual);
    }
}
