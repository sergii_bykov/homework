package com.deveducation.task.task1;

import com.deveducation.task.task1.Array;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ArrayTest {


    @Test
    public void minElement() {
        int result = Array.minElement(new int[]{6, 7, 89, 9});
        assertEquals(result, 6);

    }

    @Test
    public void maxElement() {
        int result = Array.maxElement(new int[]{6, 7, 89, 9});
        assertEquals(result, 89);

    }

    @Test
    public void indexMinElement() {
        int result = Array.indexMinElement(new int[]{6, 7, 89, 9});
        assertEquals(result, 0);

    }

    @Test
    public void indexMaxElement() {
        int result = Array.indexMaxElement(new int[]{6, 7, 89, 9});
        assertEquals(result, 2);

    }

    @Test
    public void sumElementsOddIndex() {
        int result = Array.sumElementsOddIndex(new int[]{6, 7, 1, 9, 78, 101, 1, 5, 4});
        assertEquals(result, 122);

    }

    @Test
    public void reversArray() {
        int[] result = Array.reversArray(new int[]{6, 7, 1, 9, 78, 101, 1, 5, 4});
        assertArrayEquals(result, new int[]{4, 5, 1, 101, 78, 9, 1, 7, 6});

    }

    @Test
    public void countOddElements() {
        int result = Array.countOddElements(new int[]{5, 7, 1, 9, 78, 101, 1, 5, 4});
        assertEquals(result, 7);

    }

    @Test
    public void replaceHalfArray() {
        int[] result = Array.replaceHalfArray(new int[]{5, 7, 1, 9, 12, 101, 1, 5});
        assertArrayEquals(result, new int[]{12, 101, 1, 5, 5, 7, 1, 9});

    }

    @Test
    public void selectionSort() {
        int[] result = Array.selectionSort(new int[]{5, 7, 1, 9, 12, 101, 1});
        assertArrayEquals(result, new int[]{1, 1, 5, 7, 9, 12, 101});

    }

    @Test
    public void insertionSort() {
        int[] result = Array.insertionSort(new int[]{5, 7, 1, 9, 12});
        assertArrayEquals(result, new int[]{1, 5, 7, 9, 12});

    }
}
