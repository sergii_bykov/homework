package com.deveducation.task.task1;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class CyclesTest {


    @Test
    public void sumAndCountEvenElements6() {
        int[] expected = new int[]{2450, 50};
        int[] actual = Сycles.sumAndCountEvenElements6();
        assertArrayEquals(expected, actual);

    }

    @Test
    public void isSimpleNumber7() {
        boolean result = Сycles.isSimpleNumber7(5);
        assertEquals(result, true);

    }

    @Test
    public void mySqrtChecking() {
        int result = Сycles.mySqrtChecking(9);
        assertEquals(result, 3);

    }

    @Test
    public void mySqrtBinSearch() {
        int result = Сycles.mySqrtBinSearch(9);
        assertEquals(result, 3);

    }

    @Test
    public void myFactorial() {
        int result = Сycles.myFactorial(4);
        assertEquals(result, 24);

    }

    @Test
    public void sumOfNumbers() {
        int result = Сycles.sumOfNumbers(931);
        assertEquals(result, 13);

    }

    @Test
    public void reversNumbers() {
        int result = Сycles.reversNumbers(97609);
        assertEquals(result, 90679);

    }
}
