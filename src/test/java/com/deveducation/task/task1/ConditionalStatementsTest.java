package com.deveducation.task.task1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConditionalStatementsTest {


    @Test
    public void evenOdd1Test() {
        int result = ConditionalStatements.evenOdd1(1, 5);
        assertEquals(result, 6);

    }

    @Test
    public void evenOdd12Test() {
        int result = ConditionalStatements.evenOdd1(2, 5);
        assertEquals(result, 10);

    }

    @Test
    public void quarter2() {
        int result = ConditionalStatements.quarter2(2, 5);
        assertEquals(result, 1);

    }

    @Test
    public void sumOfPositiveElements3() {
        int result = ConditionalStatements.sumOfPositiveElements3(2, 5, 7);
        assertEquals(result, 14);

    }

    @Test
    public void expression4() {
        int result = ConditionalStatements.expression4(2, 5, 7);
        assertEquals(result, 73);

    }

    @Test
    public void findStudentGrade5() {
        char result = ConditionalStatements.findStudentGrade5(76);
        assertEquals(result, 'B');

    }
}
